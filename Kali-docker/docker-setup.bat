@echo off

SET KALI_IMAGE=favju/kali-linux-headless:latest
SET KALI_CONTAINER_NAME=kali-docker
SET METASPLOITABLE2_IMAGE=tleemcjr/metasploitable2:latest
SET METASPLOITABLE2_CONTAINER_NAME=metasploitable2

echo Pulling images kali Image...
docker pull %KALI_IMAGE%
echo Kali image pulled 
echo.
echo Pulling metasploitable2 Image...
docker pull %METASPLOITABLE2_IMAGE%
echo Metasploitable2 image pulled
echo.
docker network ls --filter name=pentest-network | findstr /C:"pentest-network" > nul
if %errorlevel% neq 0 (
    echo Creating network...
    docker network create ^
      --driver=bridge ^
      --subnet=192.168.1.0/24 ^
      --ip-range=192.168.1.0/24 ^
      --gateway=192.168.1.1 ^
      pentest-network
) else (
    echo Network already exists. Skipping creation.
)
echo.
echo Starting %METASPLOITABLE2_IMAGE% in a new window...
start cmd.exe /k docker run --rm --name %METASPLOITABLE2_CONTAINER_NAME% --network pentest-network -it %METASPLOITABLE2_IMAGE% sh -c "/bin/services.sh && bash"
echo.
echo Starting %KALI_IMAGE% in a new window...
start cmd.exe /k docker run --rm --name %KALI_CONTAINER_NAME% --network pentest-network -v "%cd%/mountFolder:/app" -it %KALI_IMAGE% /bin/bash -c "cd /app; exec bash"

echo.
echo Images are running in separate windows.
echo.
echo you can run ""bash dockerPentest.sh"" in the Kali docker.
echo.
echo.
echo Press any key to stop the containers...
pause > nul
echo.
echo Stopping containers...
docker stop %KALI_CONTAINER_NAME% %METASPLOITABLE2_CONTAINER_NAME%